import React from "react";
import Banner from "../components/Football/Banner";
import NextMatch from "../components/Football/NextMatch";
import UpcomingMatches from "../components/Football/UpcomingMatches";
import MatcheHighlights from "../components/Football/MatcheHighlights";
import Products from "../components/Football/Products";
import Partners from "../components/Football/Partners";
import Gallery from "../components/Football/Gallery";
import Subscribe from "../components/Football/Subscribe";
import BlogPost from "../components/Football/BlogPost";
import About from "../components/Football/About";
import Trainer from "../components/Football/Trainer";
import Schedule from "../components/Football/Schedule";

const Home = () => {
  return (
    <React.Fragment>
      <Banner />
      <NextMatch />
      <Schedule />
      <UpcomingMatches />
      <MatcheHighlights />
      <Products />
      <About />
      <Trainer />
      <Partners />
      <Gallery />
      <Subscribe />
      <BlogPost />
    </React.Fragment>
  );
};

export default Home;
