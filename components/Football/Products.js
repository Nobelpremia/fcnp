import React from "react";
import Link from "next/link";

const Products = () => {
  return (
    <section id="shop" className="products-area pt-100 pb-70">
      <div className="container">
        <div className="section-title">
          <h2>Магазин</h2>
          <p>
            В тесной кооперации с нашими партнерами мы производим сувениры,
            одежду, футбольный инвентарь и прочие изделия с символикой школы. Вы
            можете заказать товары с доставкой в нашем магазине.
          </p>
        </div>

        <div className="row">
          <div className="col-lg-3 col-sm-6">
            <div className="single-products-box">
              <img
                src="images/football/products/footb-product1.jpg"
                alt="image"
              />

              <div className="content">
                <h3>Бутсы Adidas</h3>
                <p>
                  Профессиональные бутсы для детей и подростков в возрасте от 3
                  до 18 лет.
                </p>
                <Link href="#">
                  <a className="shop-now-btn">Купить</a>
                </Link>
              </div>

              <Link href="#">
                <a target="_blank" className="link-btn"></a>
              </Link>
            </div>
          </div>

          <div className="col-lg-3 col-sm-6">
            <div className="single-products-box">
              <img
                src="images/football/products/footb-product2.jpg"
                alt="image"
              />

              <div className="content">
                <h3>Бутсы зеленые</h3>
                <p>Очень зеленые бутсы для футбола.</p>

                <Link href="#">
                  <a className="shop-now-btn">Купить</a>
                </Link>
              </div>

              <Link href="#">
                <a target="_blank" className="link-btn"></a>
              </Link>
            </div>
          </div>

          <div className="col-lg-3 col-sm-6">
            <div className="single-products-box">
              <img
                src="images/football/products/footb-product3.jpg"
                alt="image"
              />

              <div className="content">
                <h3>Бутсы синие</h3>
                <p>Очень синие бутсы для футбола.</p>

                <Link href="#">
                  <a className="shop-now-btn">Купить</a>
                </Link>
              </div>

              <Link href="#">
                <a target="_blank" className="link-btn"></a>
              </Link>
            </div>
          </div>

          <div className="col-lg-3 col-sm-6">
            <div className="single-products-box">
              <img
                src="images/football/products/footb-product4.jpg"
                alt="image"
              />

              <div className="content">
                <h3>Форма Adidas</h3>
                <p>
                  Профессиональная полосатая форма для детей и подростков от 3
                  до 18 лет.
                </p>

                <Link href="#">
                  <a className="shop-now-btn">Купить</a>
                </Link>
              </div>

              <Link href="#">
                <a target="_blank" className="link-btn"></a>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Products;
