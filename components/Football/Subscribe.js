import React from "react";
import Link from "next/link";

const Subscribe = () => {
  return (
    <section className="subscribe-area">
      <div className="container">
        <div className="subscribe-inner-area">
          <div className="section-title">
            <h2>Подписка</h2>
            <p>
              Подпишитесь на наши новости и будете всегда в курсе самых свежих
              футбольных новостей, а также изменений расписания и тд.
            </p>
          </div>

          <form className="newsletter-form">
            <input
              type="email"
              className="input-newsletter"
              placeholder="Введите Ваш email"
              name="email"
              required
              disabled
            />
            <button type="submit" disabled>
              Подписаться
            </button>

            <div className="check-info">
              <input className="inp-cbx" id="cbx" type="checkbox" disabled />
              <label className="cbx" htmlFor="cbx">
                <span>
                  <svg width="12px" height="9px" viewBox="0 0 12 9">
                    <polyline points="1 5 4 8 11 1"></polyline>
                  </svg>
                </span>
                <span>
                  Я прочитал и согласился с{" "}
                  <Link href="#">
                    <a>пользовательским соглашением.</a>
                  </Link>
                </span>
              </label>
            </div>
          </form>

          <div className="subscribe-shape1">
            <img src="images/football/football1.png" alt="image" />
          </div>
          <div className="subscribe-shape2">
            <img src="images/football/football2.png" alt="image" />
          </div>
        </div>
      </div>
    </section>
  );
};

export default Subscribe;
