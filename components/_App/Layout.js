import React from "react";
import Head from "next/head";
import { useRouter } from "next/router";
import Navbar from "../Layout/Navbar";
import Footer from "../Layout/Footer";
import GoTop from "../Shared/GoTop";

const Layout = ({ children }) => {
  const router = useRouter();
  return (
    <React.Fragment>
      <Head>
        <title>Футбольная школа - Новое поколение</title>
        <meta
          name="description"
          content="Футбольная школа - Новое поколение. "
        />
        <meta
          name="og:title"
          property="og:title"
          content="Футбольная школа - Новое поколение"
        ></meta>
        <meta
          name="twitter:card"
          content="Футбольная школа - Новое поколение"
        ></meta>
        <link rel="canonical" href="https://fcnew-pokolenie.ru/"></link>
      </Head>
      {router.pathname === "/football-single-blog" ? null : <Navbar />}
      {children}
      <Footer />
      <GoTop scrollStepInPx="100" delayInMs="10.50" />
    </React.Fragment>
  );
};

export default Layout;
